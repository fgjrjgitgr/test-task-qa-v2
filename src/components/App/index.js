import React from 'react';
import {
    BrowserRouter as Router,
    Route,
} from 'react-router-dom';
import { Redirect } from 'react-router-dom';
import SignUpPage from '../SignUp';
import SignInPage from '../SignIn';
import ComparePage from '../Compare';
import { withAuthentication } from '../Session';
import * as ROUTES from '../../constants/routes';

const App = ({ auth }) => {
  return (
    <Router>
      <Route exact path={ROUTES.ROOT}>
          <Redirect to={ROUTES.SIGN_IN} />
      </Route>
      <Route path={ROUTES.SIGN_UP} component={SignUpPage} />
      <Route path={ROUTES.SIGN_IN} component={SignInPage} />
      <Route path={ROUTES.COMPARE} component={ComparePage} />
  </Router>
  );
}


export default withAuthentication(App);